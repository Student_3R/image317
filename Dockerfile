From quay.io/ansible/awx-ee:21.13.0
# RUN pip install --upgrade git+https://github.com/vmware/vsphere-automation-sdk-python.git
RUN pip install cryptography
# RUN pip install pyvmomi 
RUN ansible-galaxy collection install community.windows
RUN ansible-galaxy collection install community.general
RUN ansible-galaxy collection install community.vmware
RUN pip install requests-credssp
